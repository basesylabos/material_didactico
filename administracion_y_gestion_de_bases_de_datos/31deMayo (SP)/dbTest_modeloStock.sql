SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `dbTest` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `dbTest` ;

-- -----------------------------------------------------
-- Table `dbTest`.`Provincia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Provincia` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Provincia` (
  `idProvincia` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NULL ,
  PRIMARY KEY (`idProvincia`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Cliente` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Cliente` (
  `codCliente` VARCHAR(20) NOT NULL ,
  `razonSocial` VARCHAR(45) NULL ,
  `contacto` VARCHAR(45) NULL ,
  `direccion` VARCHAR(45) NULL ,
  `telefono` VARCHAR(45) NULL ,
  `codPost` VARCHAR(10) NULL ,
  `porcDescuento` DECIMAL(10,2) NULL ,
  `Provincia_idProvincia` INT NOT NULL ,
  PRIMARY KEY (`codCliente`) ,
  INDEX `fk_Cliente_Provincia_idx` (`Provincia_idProvincia` ASC) ,
  CONSTRAINT `fk_Cliente_Provincia`
    FOREIGN KEY (`Provincia_idProvincia` )
    REFERENCES `dbTest`.`Provincia` (`idProvincia` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Proveedor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Proveedor` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Proveedor` (
  `idProveedor` INT NOT NULL AUTO_INCREMENT ,
  `razonSocial` VARCHAR(45) NULL ,
  `contacto` VARCHAR(45) NULL ,
  `direccion` VARCHAR(45) NULL ,
  `telefono` VARCHAR(45) NULL ,
  `codPost` VARCHAR(10) NULL ,
  `Provincia_idProvincia` INT NOT NULL ,
  PRIMARY KEY (`idProveedor`) ,
  INDEX `fk_Proveedor_Provincia1_idx` (`Provincia_idProvincia` ASC) ,
  CONSTRAINT `fk_Proveedor_Provincia1`
    FOREIGN KEY (`Provincia_idProvincia` )
    REFERENCES `dbTest`.`Provincia` (`idProvincia` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Categoria` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NULL ,
  PRIMARY KEY (`idCategoria`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Producto` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Producto` (
  `codProducto` VARCHAR(20) NOT NULL ,
  `descripcion` VARCHAR(100) NULL ,
  `precio` DECIMAL(10,2) NULL ,
  `Categoria_idCategoria` INT NOT NULL ,
  PRIMARY KEY (`codProducto`) ,
  INDEX `fk_Producto_Categoria1_idx` (`Categoria_idCategoria` ASC) ,
  CONSTRAINT `fk_Producto_Categoria1`
    FOREIGN KEY (`Categoria_idCategoria` )
    REFERENCES `dbTest`.`Categoria` (`idCategoria` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Producto_Proveedor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Producto_Proveedor` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Producto_Proveedor` (
  `Producto_codProducto` VARCHAR(20) NOT NULL ,
  `Proveedor_idProveedor` INT NOT NULL ,
  `precio` DECIMAL(10,2) NULL ,
  `demoraEntrega` INT NULL ,
  PRIMARY KEY (`Producto_codProducto`, `Proveedor_idProveedor`) ,
  INDEX `fk_Producto_has_Proveedor_Proveedor1_idx` (`Proveedor_idProveedor` ASC) ,
  INDEX `fk_Producto_has_Proveedor_Producto1_idx` (`Producto_codProducto` ASC) ,
  CONSTRAINT `fk_Producto_has_Proveedor_Producto1`
    FOREIGN KEY (`Producto_codProducto` )
    REFERENCES `dbTest`.`Producto` (`codProducto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Producto_has_Proveedor_Proveedor1`
    FOREIGN KEY (`Proveedor_idProveedor` )
    REFERENCES `dbTest`.`Proveedor` (`idProveedor` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Producto_Ubicacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Producto_Ubicacion` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Producto_Ubicacion` (
  `idProductoUbicacion` INT NOT NULL AUTO_INCREMENT ,
  `cantidad` INT NULL ,
  `estanteria` VARCHAR(45) NULL ,
  `Producto_codProducto` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`idProductoUbicacion`) ,
  INDEX `fk_Producto_Ubicacion_Producto1_idx` (`Producto_codProducto` ASC) ,
  CONSTRAINT `fk_Producto_Ubicacion_Producto1`
    FOREIGN KEY (`Producto_codProducto` )
    REFERENCES `dbTest`.`Producto` (`codProducto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`IngresoStock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`IngresoStock` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`IngresoStock` (
  `idIngreso` INT NOT NULL AUTO_INCREMENT ,
  `fecha` DATETIME NULL ,
  `remitoNro` VARCHAR(45) NULL ,
  `Proveedor_idProveedor` INT NOT NULL ,
  PRIMARY KEY (`idIngreso`) ,
  INDEX `fk_IngresoStock_Proveedor1_idx` (`Proveedor_idProveedor` ASC) ,
  CONSTRAINT `fk_IngresoStock_Proveedor1`
    FOREIGN KEY (`Proveedor_idProveedor` )
    REFERENCES `dbTest`.`Proveedor` (`idProveedor` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`IngresoStock_Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`IngresoStock_Producto` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`IngresoStock_Producto` (
  `IngresoStock_idIngreso` INT NOT NULL ,
  `item` INT NOT NULL ,
  `Producto_codProducto` VARCHAR(20) NOT NULL ,
  `cantidad` INT NULL ,
  PRIMARY KEY (`IngresoStock_idIngreso`, `item`) ,
  INDEX `fk_IngresoStock_has_Producto_Producto1_idx` (`Producto_codProducto` ASC) ,
  INDEX `fk_IngresoStock_has_Producto_IngresoStock1_idx` (`IngresoStock_idIngreso` ASC) ,
  CONSTRAINT `fk_IngresoStock_has_Producto_IngresoStock1`
    FOREIGN KEY (`IngresoStock_idIngreso` )
    REFERENCES `dbTest`.`IngresoStock` (`idIngreso` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_IngresoStock_has_Producto_Producto1`
    FOREIGN KEY (`Producto_codProducto` )
    REFERENCES `dbTest`.`Producto` (`codProducto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Estado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Estado` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Estado` (
  `idEstado` INT NOT NULL ,
  `nombre` VARCHAR(45) NULL ,
  PRIMARY KEY (`idEstado`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Pedido`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Pedido` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Pedido` (
  `idPedido` INT NOT NULL AUTO_INCREMENT ,
  `fecha` DATETIME NULL ,
  `Estado_idEstado` INT NOT NULL ,
  `Cliente_codCliente` VARCHAR(20) NOT NULL ,
  PRIMARY KEY (`idPedido`) ,
  INDEX `fk_Pedido_Estado1_idx` (`Estado_idEstado` ASC) ,
  INDEX `fk_Pedido_Cliente1_idx` (`Cliente_codCliente` ASC) ,
  CONSTRAINT `fk_Pedido_Estado1`
    FOREIGN KEY (`Estado_idEstado` )
    REFERENCES `dbTest`.`Estado` (`idEstado` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedido_Cliente1`
    FOREIGN KEY (`Cliente_codCliente` )
    REFERENCES `dbTest`.`Cliente` (`codCliente` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbTest`.`Pedido_Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbTest`.`Pedido_Producto` ;

CREATE  TABLE IF NOT EXISTS `dbTest`.`Pedido_Producto` (
  `Pedido_idPedido` INT NOT NULL ,
  `item` INT NOT NULL ,
  `Producto_codProducto` VARCHAR(20) NOT NULL ,
  `cantidad` INT NOT NULL ,
  `precioUnitario` DECIMAL(10,2) NOT NULL ,
  PRIMARY KEY (`Pedido_idPedido`, `item`) ,
  INDEX `fk_Pedido_has_Producto_Producto1_idx` (`Producto_codProducto` ASC) ,
  INDEX `fk_Pedido_has_Producto_Pedido1_idx` (`Pedido_idPedido` ASC) ,
  CONSTRAINT `fk_Pedido_has_Producto_Pedido1`
    FOREIGN KEY (`Pedido_idPedido` )
    REFERENCES `dbTest`.`Pedido` (`idPedido` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedido_has_Producto_Producto1`
    FOREIGN KEY (`Producto_codProducto` )
    REFERENCES `dbTest`.`Producto` (`codProducto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `dbTest` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
