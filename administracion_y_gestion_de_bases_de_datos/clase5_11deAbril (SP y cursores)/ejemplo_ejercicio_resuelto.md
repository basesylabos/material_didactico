----
### Stored Procedures - Cursores
---

-----
#### Resolución del ejercicio 1
-----

##### Aviso: el enunciado fué modificado con fines didácticos. 

- Definir un SP que reciba un id correspondiente a una productora. El SP deberá devolver los nombres de todas las películas que produjo y tengan como integrantes a actores cuyos nombres artísticos contengan las letras “a”, “b” o “c”. Los nombres deberán estar separados por un guión medio.


- Recuerden que hay varias formas de realizar el ejercicio. Les propongo que piensen
una forma distinta de realizar el ejercicio.


```sql
CREATE PROCEDURE obtener_nombres_de_peliculas(IN id_productora INT, OUT lista_nombres_de_peliculas VARCHAR(3000))
BEGIN

  DECLARE done INT DEFAULT FALSE;
  
  DECLARE nombre_obtenido VARCHAR(100);
  
  DECLARE cur1 CURSOR FOR SELECT pelicula.nombre 
                          FROM pelicula INNER JOIN actor
                          ON pelicula.id_pelicula = actor.id_pelicula
                          WHERE pelicula.id_productora = id_productora 
                                AND 
                                ( actor.nombre_artistico LIKE "%a%" OR 
                                  actor.nombre_artistico LIKE "%b%" OR 
                                  actor.nombre_artistico LIKE "%c%" );

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  SET resultado = "";

  OPEN cur1;

  concatenar_nombres: LOOP
  
    FETCH cur1 INTO nombre_obtenido;
    
    IF done THEN
      LEAVE concatenar_nombres;
    END IF;

    SET resultado = CONCAT(resultado,nombre_obtenido,"_");
    
  END LOOP;

  CLOSE cur1;

  -- Le sacamos el último guión:

  DECLARE cantidad_de_elementos;

  SET cantidad_de_elementos = LENGTH(resultado);
  SET resultado = SUBSTRING(resultado,1,cantidad_de_elementos - 1);


END;

```

