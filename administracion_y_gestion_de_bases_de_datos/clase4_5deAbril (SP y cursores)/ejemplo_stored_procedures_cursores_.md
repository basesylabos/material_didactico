----
#### Ejemplo - Stored Procedure - Cursores
----

- Tabla PERSONA.

  |  id |   nombre   |  edad  |
  |-----|------------|--------|
  |  1  | "Gerardo"  |  39    |
  |  2  | "Melina"   |  78    |
  |  3  | "Andrea"   |  45    |
  

-----
#### Ejemplo 1
----

- Objetivo: definir un SP que obtener los nombres de la tabla unidos mediante un guión bajo.

- Resultado esperado: ```"Gerardo_Melina_Andrea"```



```sql
CREATE PROCEDURE generar_listado_de_nombres(OUT resultado VARCHAR(2000))
BEGIN

  DECLARE done INT DEFAULT FALSE;
  
  DECLARE nombre_obtenido VARCHAR(100);
  
  DECLARE cur1 CURSOR FOR SELECT nombre FROM persona;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  SET resultado = "";

  OPEN cur1;

  concatenar_nombres: LOOP
  
    FETCH cur1 INTO nombre_obtenido;
    
    IF done THEN
      LEAVE concatenar_nombres;
    END IF;

    SET resultado = CONCAT(resultado,nombre_obtenido,"_");
    
  END LOOP;

  CLOSE cur1;
END;

```

-----
#### Ejemplo 2
----

- Objetivo: definir un SP que obtener los nombres y su edades de la tabla. Los datos deberán estar unidos mediante un punto.

- Resultado esperado: ```"Gerardo.39.Melina.78.Andrea.45"```

```sql
CREATE PROCEDURE generar_listado_de_nombres_con_edades(OUT resultado VARCHAR(3000))
BEGIN

  DECLARE done INT DEFAULT FALSE;
  
  DECLARE nombre_obtenido VARCHAR(100);
  DECLARE edad_obtenida INT;
  
  DECLARE cur1 CURSOR FOR SELECT nombre, edad FROM persona;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  SET resultado = "";
  
  OPEN cur1;

  concatenar_nombres_y_edades: LOOP
  
    FETCH cur1 INTO nombre_obtenido,edad_obtenida;
    
    IF done THEN
      LEAVE concatenar_nombres_y_edades;
    END IF;

    SET resultado = CONCAT(resultado,nombre_obtenido,".",edad_obtenida,".");
    
  END LOOP;

  CLOSE cur1;
END;

```

-----
#### Ejemplo 3
----

- Objetivo: definir un SP que obtener los nombres y su edades de aquellas personas que tengan un id impar. Los datos de cada persona deberán estar unidos mediante un guión medio y cada nombre y edad deberá estar separado por un punto.

- Resultado esperado: ```"Gerardo.39-Andrea.45"```

```sql
CREATE PROCEDURE obtener_datos_de_personas_con_ids_impares(OUT resultado VARCHAR(3000))
BEGIN

  DECLARE done INT DEFAULT FALSE;
  
  DECLARE nombre_obtenido VARCHAR(100);
  DECLARE edad_obtenida INT;
  DECLARE id_obtenido INT;
  
  DECLARE cur1 CURSOR FOR SELECT id, nombre, edad FROM persona;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  SET resultado = "";

  OPEN cur1;

  concatenar_nombres_y_edades: LOOP
  
    FETCH cur1 INTO id_obtenido,nombre_obtenido,edad_obtenida;
    
    IF done THEN
      LEAVE concatenar_nombres_y_edades;
    END IF;

    IF id_obtenido % 2 = 1 THEN 
      SET resultado = CONCAT(resultado,nombre_obtenido,".",edad_obtenida,"-");
    END IF;
    
  END LOOP;

  CLOSE cur1;
END;

```



