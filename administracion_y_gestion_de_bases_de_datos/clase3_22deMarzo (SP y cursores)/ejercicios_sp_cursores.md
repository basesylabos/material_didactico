


### Administración y Gestión de Bases de Datos

---

#### Stored Procedures / Procedimientos almacenados

#### Cursors / Cursores

---

##### Material utilizado · Documentación Oficial de MySQL

- [Stored Procedures](https://dev.mysql.com/doc/refman/8.0/en/create-procedure.html)

- [Cursors](https://dev.mysql.com/doc/refman/8.0/en/cursors.html)

- [IF](https://dev.mysql.com/doc/refman/8.0/en/if.html)

- [WHILE](https://dev.mysql.com/doc/refman/8.0/en/while.html)

---

#### Guía de ejercicios

-  Dado el modelo de datos llamado classicmodels, importar la base de datos proporcionada
  (base_de_datos_classicmodels.sql) y luego definir las siguientes vistas.


1. Crear un SP que liste todos los productos que tengan un precio de compra mayor al precio promedio y 
que devuelva la cantidad de productos que cumplan con esa condición.

2. Crear un SP que reciba un orderNumber. El SP deberá borrar la orden correspondiente a ese orderNumber
y todos los ítems de la tabla orderDetails asociados a él. El SP deberá devolver 0 si no encontró filas para ese orderNumber; en caso de que sí haya encontrado filas para ese ordenNumber, deberá devolver la cantidad ítems borrados.

3. Crear un SP que reciba un identificador de producLine y luego borre la línea de productos de la tabla Productlines. Tenga en cuenta que la línea de productos no podrá ser borrada si tiene productos asociados. El procedure debe devolver un mensaje que contenga una de las siguientes leyendas:

    - La línea de productos fue borrada

    - La línea de productos no pudo borrarse porque contiene productos asociados. 

4. Realizar un SP que liste la cantidad de ordenes que hay por estado.   

5. Al procedimiento anterior, agreguele dos parametros de entrada correspondientes a dateOrder. 
Los dos parámetros deberán llamarse _desde_ y _hasta_. El procedimiento tendrá que 
listar la cantidad de órdenes por estado que se encuentren entre esas dos fechas.

6. Realizar un SP que modifique el campo comments de la tabla orders. El procedimiento 
recibe un orderNumber y el comentario. El procedimiento deberá devolver 1 si se encontró la orden 
y se modifico; y deberá devolver 0 en caso contrario. 

7. Crear un SP que utilice un cursor para recorrer la tabla de offices y generar 
una lista con las ciudades en las cuales hay oficinas. La lista tendrá que devolverse 
en un parámetro de salida VARCHAR(4000) que contenga todas las ciudades separadas 
por coma. El nombre del procedimiento deberá ser _getCiudadesOffices_.

8. Agregue una tabla llamada CancelledOrders con el mismo diseño que la tabla de Orders. Crear un SP que recorra la tabla de orders y que cuente la cantidad de ordenes en estado cancelled. El procedimiento deberá insertar una fila en la tabla CancelledOrders por cada orden cancelada y deberá devolver la cantidad de ordenes canceladas. El nombre del procedimiento deberá ser _insertCancelledOrders_.

9. Realizar un SP que reciba un dato que represente el customerNumber de un cliente. Para todas las ordenes de ese customerNumber, el procedimiento deberá completar el campo comments en caso de que esté vacío. El comentario a asignar deberá ser: "El total de la orden es x "; donde x representará el total de la orden, valor  que deberá calcularse sumando el precio de todos los productos incluidos en la orden de la tabla OrderDetails. 
El nombre del procedimiento deberá ser _alterCommentOrder_. 


---
#### Comando para iniciar mysql mediante consola

```sql
  mysql -u alumno -p
```
---