----
#### Stored Procedures - Cursores 
----

##### Ejercicios prácticos

----

- Realizar los siguientes ejercicios en papel.


1. Definir una variable de tipo int que sirva para almacenar una edad. Suponer que la variable será creada dentro de un procedimiento almacenado (Stored Procedure).


2. Definir una variable de sesión que almacene la palabra "cuaderno".


3. Definir un SP (Stored Procedure) que imprima la frase "Buenos días".


4. Definir un SP (Stored Procedure) que reciba un número entero y luego lo imprima.


5. Definir un SP (Stored Procedure) que reciba un número entero y luego imprima 
una duplicación del mismo.


6. Definir un SP (Stored Procedure) que reciba un número entero y luego devuelva 
una triplicación del mismo.


7. Definir un SP (Stored Procedure) que reciba una palabra y luego devuelva una 
cadena compuesta por 3 repeticiones de la palabra recibida.


8. Definir un SP (Stored Procedure) que reciba una palabra y una variable
llamada n que represente una cantidad de repeticiones.
El SP deberá devolver una cadena compuesta por n repeticiones
de la palabra recibida. Utilizar un ciclo while.

  - Ejemplo de procedimiento que utiliza un cilo while:

```sql
CREATE PROCEDURE dowhile()
BEGIN
  DECLARE v1 INT DEFAULT 5;

  WHILE v1 > 0 DO
    SELECT v1;
    SET v1 = v1 - 1;
  END WHILE;
END;
```

- El ejemplo fue extraído de la [documentacion oficial de MySQL](https://dev.mysql.com/doc/refman/8.0/en/while.html).

9. Definir un SP (Stored Procedure) que seleccione el nombre y el apellido de las
personas que tienen más de 18 años.


10. Definir un SP (Stored Procedure) que reciba una edad y luego seleccione los 
nombres de las personas que tengan una edad mayor o igual a 
la edad recibida.


11. Responder con sus palabras: ¿Qué es FETCH?¿Para qué sirve?.


12. Responder con sus palabras: ¿Para qué sirve la siguiente línea?

```sql
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
```

