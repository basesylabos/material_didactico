----
#### Stored Procedures
----
##### Ejercicios prácticos
----

- Utilizando el modelo Classicmodels, realizar los siguientes ejercicios.

1. Crear un SP que reciba un año y luego muestre la fecha de orden,
los comentarios de la órden y el identificador del cliente de aquellas órdenes realizadas
en en año recibido. El SP no deberá devolver valores. Realice el llamado al procedimiento.

2. Crear un SP que devuelva la fecha en la que se realizaron más órdenes. 
Realice el llamado al procedimiento.


3. Crear un SP que reciba un identificador de productline y luego elimine
a todos los productos asociados al mismo. Realice el llamado al procedimiento.


4. Crear un SP que reciba un employeeNumber y luego devuelva un listado de aquellos 
curtomerNumber de clientes que hayan realizado órdenes un día
antes de navidad. Realice el llamado al procedimiento.


5. Crear un SP que reciba un customerNumber. Si el cliente fue atendido por un empleado,
el SP deberá devolver el officeCode de la oficina en la que trabaja el empleado. Si el
cliente no fue atendido por un empleado, el SP deberá devolver "No office". Tener
en cuenta de que si un cliente no fué atendido por un cliente, entonces en el campo
salesRepEmployeeNumber tendrá el valor NULL. Realice el llamado al procedimiento.


6. Crear un SP que devuelva los nombres de aquellos productos que fueron ordenados
con cantidades mayores o iguales a 40 unidades. Utilizar DISTINCT para evitar
nombres repetidos. Realice el llamado al procedimiento.


7. Crear un SP que duplique el buyPrice de aquellos productos que tienen menos de
700 unidades disponibles y que cada uno esté asociado a un orderNumber que sea par.
Realice el llamado al procedimiento.
