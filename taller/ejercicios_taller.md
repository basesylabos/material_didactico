-----
#### Taller
-----
##### Ejercicios
-----

1. Crear una tabla que funcione como una grilla de imágenes. La dimensión de la
tabla deberá ser de, al menos, 2 filas por 2 columnas. 

Al hacer click sobre cada imágen, se deberá abrir una nueva pestaña donde se deberá encontrar 
la imágen clickeada con un tamaño que ocupe el 50% del la pantalla. 
Para realizar este efecto, deberá utilizar el atributo target con el valor ```_blank```.

- [Ejemplo](https://developer.mozilla.org/es/docs/Web/HTML/Element/a#creando_una_imagen_clicable)

Además, debajo de cada imágen deberá haber un botón que contenga la frase "Ver especificaciones".
Al hacer click sobre el botón, se deberá abrir una nueva pestaña que muestre todos los
detalles del elemento que muestra la imágen. Para realizar este tipo de efecto, se deberá combinar
una etiqueta de hipervínculo con una etiqueta button.

2. Crear un botón que se ubique a en la parte superior derecha de la página principal. 
Este botón deberá contener la frase "Registrarse". Al hacer click sobre este botón, se
deberá abrir una nueva pestaña que muestre un formulario que pida los siguientes datos:

	- Nombre
	- Apellido
	- Edad (para este campo se deberán utilizar las etiquetas select y option, las edades ofrecidas
	deberán ser desde los 13 años hasta los 100 años)
	- Email
	- Constraseña


El formulario también deberá tener un área para el usuario ingrese comentarios y, finalmente,
un botón que contenga la frase "Completar registro". 
Luego, al hacer click sobre el botón, se deberá mostrar una frase que diga "¡Muchas gracias
por registrarse en nuestro sitio web!". Esta nueva página deberá tener un botón que sirva
para dirigir al usuario a la página principal. Notar que esta vez, luego de que el usuario
complete el formulario, no se deberá abrir una nueva pestaña.

