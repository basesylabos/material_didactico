-----
#### Taller
-----
##### Material de referencia
-----

- Tipos de sitios web

    - Sitios estáticos
    
        - Ejemplos: 
            - [https://www.uba.ar/](https://www.uba.ar/)
            - [https://www.ipm.edu.ar/](https://www.ipm.edu.ar/)
        
    - Sitios dinámicos
    
        - Ejemplos: 
            - [https://campus.exactas.uba.ar/](https://campus.exactas.uba.ar/)
            - [https://www.instagram.com/](https://www.instagram.com/)
            - [https://www.amazon.com/-/es/](https://www.amazon.com/-/es/)
            - [https://www.youtube.com/](https://www.youtube.com/)

----
- ### HTML
----

- #### [Estructura básica de un sitio web](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Document_and_website_structure)
- #### [Anatomía de un elemento HTML](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Getting_started#anatom%C3%ADa_de_un_elemento_html)
- #### [Head](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/The_head_metadata_in_HTML)
- #### [Heading elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements)
- #### [Párrafos](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/HTML_text_fundamentals#conceptos_b%C3%A1sicos_encabezados_y_p%C3%A1rrafos)
- #### [Efecto cursiva](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em#examples)
- #### [Efecto negrita](https://developer.mozilla.org/es/docs/Web/HTML/Element/b#ejemplos_de_uso)
- #### [Efecto tachado](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/s#examples)
- #### [Salto de linea](https://developer.mozilla.org/es/docs/Web/HTML/Element/br)
- #### [Imágen](https://developer.mozilla.org/es/docs/Web/HTML/Element/img#ejemplo_1)
- #### [Enlace](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Creating_hyperlinks#anatom%C3%ADa_de_un_enlace)
- #### [target="_blank"](https://developer.mozilla.org/es/docs/Web/HTML/Element/a#creando_una_imagen_clicable)
- #### [Colocar una imágen como enlace](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Creating_hyperlinks#convertir_bloques_de_contenido_en_enlaces)
- #### [Enlace a correo electrónico](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Creating_hyperlinks#enlace_a_correo_electr%C3%B3nico)
- #### [Lista no ordenada](https://developer.mozilla.org/es/docs/Web/HTML/Element/ul#ejemplos_de_uso)
- #### [Lista ordenada](https://developer.mozilla.org/es/docs/Web/HTML/Element/ol#ejemplos)
- #### [Table](https://developer.mozilla.org/es/docs/Web/HTML/Element/table#ejemplos)
- #### [Style](https://developer.mozilla.org/es/docs/Web/HTML/Element/style)
- #### [Link](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link#try_ithttps://developer.mozilla.org/en-US/docs/Web/HTML/Element/link#try_it)

- #### [Form](https://developer.mozilla.org/es/docs/Learn/Forms/Your_first_form#aprendizaje_activo_la_implementaci%C3%B3n_de_nuestro_formulario_html)
- #### [Label](https://developer.mozilla.org/es/docs/Web/HTML/Element/label#ejemplos)
- #### [Input](https://developer.mozilla.org/es/docs/Web/HTML/Element/input#html)
    - [Texto](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/text)
    - [Numeros](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/number)
    - [Contraseñas](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/password)
    - [Email](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/email)


- #### [Textarea](https://developer.mozilla.org/es/docs/Web/HTML/Element/textarea#contenido_html)
- #### [Button](https://developer.mozilla.org/es/docs/Web/HTML/Element/button#ejemplos_de_uso)
- #### [Select y Option](https://developer.mozilla.org/es/docs/Web/HTML/Element/select#ejemplos)


-----
- ### CSS
-----

- #### [**:root**](https://developer.mozilla.org/es/docs/Web/CSS/:root)
    - [Ejemplo](https://developer.mozilla.org/es/docs/Web/CSS/Using_CSS_custom_properties#uso_b%C3%A1sico)


- #### [text-align](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align)
- #### [color](https://developer.mozilla.org/en-US/docs/Web/CSS/color)
- #### [font-size](https://developer.mozilla.org/en-US/docs/Web/CSS/font-size)
- #### [position: static](https://developer.mozilla.org/es/docs/Web/CSS/position#static)
- #### [position: absolute](https://developer.mozilla.org/es/docs/Web/CSS/position#absolute)
- #### [position: relative](https://developer.mozilla.org/es/docs/Web/CSS/position#relative)
- #### [**class**](https://developer.mozilla.org/es/docs/Web/CSS/Class_selectors)
- #### [**id**](https://developer.mozilla.org/en-US/docs/Web/CSS/ID_selectors)
- #### [background](https://developer.mozilla.org/es/docs/Web/CSS/background)
- #### [list-style](https://developer.mozilla.org/es/docs/Web/CSS/list-style)
- #### [width](https://developer.mozilla.org/en-US/docs/Web/CSS/width#try_it)
- #### [height](https://developer.mozilla.org/en-US/docs/Web/CSS/height#try_it)
- #### [Herencia](https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance#herencia)
- #### [Cascada](https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance#cascada)
- #### [The box model](https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/The_box_model#partes_de_una_caja)
    - [Margin](https://developer.mozilla.org/en-US/docs/Web/CSS/margin)
    - [Border](https://developer.mozilla.org/en-US/docs/Web/CSS/border#try_it)
    - [Padding](https://developer.mozilla.org/en-US/docs/Web/CSS/padding)
    - [Content](https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/The_box_model#partes_de_una_caja)

- #### [Border-radius](https://developer.mozilla.org/en-US/docs/Web/CSS/border-radius)
- #### [display | block, inline, inline-block](https://developer.mozilla.org/en-US/docs/Web/CSS/display)

----
- ### Fuentes
----

- #### [Google Fonts](https://fonts.google.com/)

