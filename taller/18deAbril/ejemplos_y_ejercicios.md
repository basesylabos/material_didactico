
----
#### Laboratorio de Algoritmos y Estructuras de Datos
----

##### Ejemplos y material de referencia

- [HTML](https://developer.mozilla.org/es/docs/Web/HTML)
- [CSS](https://developer.mozilla.org/es/docs/Learn/CSS)

#### Ejemplos y material de referencia 

- [Estructura web](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Document_and_website_structure)
- [Primeros pasos con HTML](https://developer.mozilla.org/es/docs/Learn/HTML/Introduction_to_HTML/Getting_started)
- [Primeros pasos con CSS](https://developer.mozilla.org/es/docs/Learn/CSS/First_steps/Getting_started)

#### ¿Cuáles son todas las etiquetas HTML disponibles?

- [Etiquetas HTML: https://devdocs.io/html-elements/](https://devdocs.io/html-elements/)


#### ¿Cuáles son todas las opciones de estilo (CSS)?

- [Atributos de estilo CSS: https://devdocs.io/css/#index](https://devdocs.io/css/#index)


#### Ejercicios

- Elegir un tema de especialización y crear un sitio web dedicado a ello. 

- El sitio web deberá tener los siguientes elementos:

	- encabezado: ```<header>```.
	- menú de navegación : ```<nav>```.
	- contenido principal: ```<main>```, con varias subsecciones (además de la barra lateral) representadas por los  elementos ```<article>```, ```<section>```, y ```<div>```.
	- barra lateral: ```<aside>```; a menudo colocada dentro de ```<main>```.
	- pie de página: ```<footer>```.

- Copiar el ejemplo visto y luego realizarle las modificaciones necesarias.

- Agregar dos imágenes dentro del contenido principal. Los tamaños de las imágenes
  deberán ser diferentes.

##### Manejo de la documentación

- Para encontrar información y ejemplos sobre cómo se usan las etiquetas vistas,
deberán hacer click en el buscador y luego colocar el nombre de la etiqueta.

- Ejercicio de práctica: buscar información sobre la etiqueta ```<section>```.
