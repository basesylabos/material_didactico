----
### Comandos básicos de GIT - Parte 1
----

- [git clone](https://www.atlassian.com/es/git/tutorials/setting-up-a-repository/git-clone)
- [git add](https://www.atlassian.com/es/git/tutorials/saving-changes)
- [git commit](https://www.atlassian.com/es/git/tutorials/saving-changes/git-commit)
- [git push](https://www.atlassian.com/es/git/tutorials/syncing/git-push)
- [git pull](https://www.atlassian.com/es/git/tutorials/syncing/git-pull)
- [git status](https://www.atlassian.com/es/git/tutorials/inspecting-a-repository)

