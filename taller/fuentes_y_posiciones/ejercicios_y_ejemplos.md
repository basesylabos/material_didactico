### Taller

1. Importar una fuente de google fonts
2. Elegir 5 imágenes diferentes y luego
posicionarlas en lugares diferentes.


### Material de referencia

- Fuentes de google: [link](https://fonts.google.com/)
- Position absolute [link](https://developer.mozilla.org/es/docs/Web/CSS/position)
- top, button, left, rigth [link](https://developer.mozilla.org/es/docs/Web/CSS/position)
