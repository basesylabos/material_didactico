-----
#### Laboratorio de Algoritmos y Estructuras de Datos • Evaluación
-----
##### Diagramas de flujo • Pseudocódigo
-----

- Proponer algoritmo que resuelvan los siguientes problemas. El algoritmo
propuesto deberá estar escrito con pseudocódigo y diagrama de flujo.

1. Problema: se necesita crear una algoritmo para una aplicación móvil.
El algoritmo deberá servir para calcular cuántos metros avanza un tractor
que se mueve durante 300 segundos a velocidad constante. Los datos
que recibirá por parte del usuario serán: la velocidad expresada en 
metros sobre segundos (v) y la posición inicial (x<sub>0</sub>). 
Tener en cuenta que la fórmula para calcular lo pedido es x = x<sub>0</sub> + v * t.


2. Problema: se necesita determinar si una persona puede registrarse en
un sitio web. Una persona podrá registrarse si es mayor a 18 años y si vive
en alguna provincia perteneciente a la región Cuyo de nuestro país.
Los datos que recibirá por parte del usuario serán: Nombre, edad, provincia, 
nombre de usuario y contraseña.

- Recordar que las provincias de la región Cuyo son: Mendoza, San Luis y San Juan.


3. Problema: se necesita crear un algoritmo que sirva para cobrarle a 
los usuarios de trenes su respectivo boleto. La aplicación estará instalada
en máquinas ubicadas en todas las estaciones de tren. El usuario deberá ingresar 
el nombre de la estación de tren en la que subirá y luego deberá ingresar
el nombre de la estación en la que se bajará. Para calcular el valor del boleto
se deberá seguir la siguiente tabla:

<div style="margin-left: 25%;
            margin-right: 25%;
            width: 60%">

| Estación de partida  | Estación de llegada | Precio  |
| -------------------- | ------------------- |---------|
| Ushuaia              | La Plata            | $3000   |
| Ushuaia              | Salta               | $6000   | 
| La Plata             | Ushuaia             | $3000   |
| La Plata             | Salta               | $3000   |
| Salta                | Ushuaia             | $6000   |
| Salta                | La Plata            | $3000   |

</div>

  

  


4. Problema: se necesita crear un algoritmo que cuente la cantidad de
personas que pasan por la entrada de un edificio. Cada persona que entra
al edificio, deberá ingresar su nombre y luego deberá ser saludado cordialmente
por la aplicación. Para finalizar el conteo, el personal de admisión
del edificio ingresará la frase "Jornada laboral finalizada".


5. Problema: se necesita crear un algoritmo que sirva para
sumar el precio de 100 productos y luego aplicarle un 25% de descuento.
El precio de cada producto será ingresado por el usuario de la aplicación
cada vez que se lo solicite. Al finalizar el ingreso, se deberá imprimir
el valor final calculado.