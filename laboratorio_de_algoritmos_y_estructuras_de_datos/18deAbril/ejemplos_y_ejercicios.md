----
#### Laboratorio de Algoritmos y Estructuras de Datos
----

##### Ejemplos


```c++

// Definición de funciones:


void saludar(){

	string nombre;
	cin >> nombre;
	cout << "Bienvenido, " << nombre << "." << endl;

}

string obtenerPrimeros10000numerosPositivos(){

	string primeros10milPositivos = "";

    for (int i = 0; i < 10000; ++i) {
        primeros10milPositivos += to_string(i);
        primeros10milPositivos += ' ';
    }

    return primeros10milPositivos;
}

// Definición de función con parámetro por copia o por valor

int duplicar(int numero){

	int resultado = numero * 2;
	return resultado;

}

// Definición de función con parámetro por referencia

void triplicar(int &numero){

	numero = numero * 3;
}

// Definición de función que recibe un vector y luego retorna el primer elemento 

int obtenerPrimerElemento(vector<int> numeros){
    int primerElemento = numeros[0];
    return primerElemento;
}


int main(){

	
	// utilización función que no recibe parámetros y no retorna datos
	saludar();




	// utilización función que no recibe parámetros y retorna un dato de tipo string
	string primerosPositivosComoCadena = obtenerPrimeros10000numerosPositivos();
	



	/*
	utilización función que recibe dato como 
	parámetro por copia/valor y retorna un
	dato de tipo entero
	*/
	int num1 = duplicar(10); // 20

	int num2 = 100;
	int resultado = duplicar(num2); // 200



	// utilización de función que recibe un dato como parámetro por referencia
	int num3 = 10;

	truplicar(num3);
	
	cout << num 3 << endl; // 30



	// vectores

	vector<int> listado_de_numeros = {4,2,1,4,2,2,1};

	vector<int> nums;
	nums.push_back(100);
	nums.push_back(20);
	nums.push_back(12);

	// nums = {100,20,12}

	nums.pop_back(); // {100,20}
	nums.pop_back(); // {100}

	// asignación

	listado_de_numeros = {1,4,1,12};

	vector<int> listado_de_numeros_2 = {1,7,66,54};
	listado_de_numeros = listado_de_numeros_2;

	// asignación de valores

	listado_de_numeros[0] = 22;
	listado_de_numeros[1] = 1;
	listado_de_numeros[2] = 4;
	listado_de_numeros[0] = listado_de_numeros[1]; // {1,1,4}

	

	// Utilización de funciones con vectores
	int primerElemento = obtenerPrimerElemento(listado_de_numeros); // 4

   return 0;
}
```

##### Ejercicios

- Ejercicios con funciones y cadenas:

	1. Definir una función que reciba un nombre como parámetro por copia y 
	   luego retorne la última letra.
	2. Definir una función que reciba un nombre como parámetro por valor y 
	   luego retorne la primera letra.
	3. Definir una función que reciba un nombre como parámetro por copia y 
	   luego retorne una cadena compuesta por las vocales presentes en el nombre.
	4. Definir una función que reciba un nombre como parámetro por copia y 
	   luego retorne la cantidad de vocales que contiene el nombre.
	5. Definir una función que reciba un número positivo como parámetro
	   por valor y luego retorne la suma de los números desde el 1 hasta
	   el número recibido. Por ejemplo, si la función recibe el número 3,
	   la misma deberá retornar el número 6 ya que 1 + 2 + 3 = 6.
	6. Definir una función que reciba un nombre como parámetro por referencia
	   y luego reemplace las vocales por espacios. 
	   	- Ejemplo: si la función recibe el nombre "Flavia", deberá modificarlo 
	   	  y convertirlo en "Fl v  ".
	7. Definir una función que reciba un nombre como parámetro por referencia.
	   La función deberá agregar un separador por cada letra, es decir, agregar
	   un guión medio entre cada letra que compone el nombre. 
	   	- Ejemplo: si la función recibe el nombre "Patricia", deberá convertirlo
	   	al siguiente dato de tipo string: "-P-a-t-r-i-c-i-a-". Notar que
	   	el separador deberá estar al inicio y al final del nombre recibido.
	 
- Ejercicios con vectores:

	8. Dentro de la función main, realizar las siguientes operaciones:

		1. Definir un vector de 5 números enteros a elección. 

		2. Definir un vector de 3 palabras a elección.

		3. Agregar los números 100, 300, 400 al vector definido en el punto 1.
		   Utilizar la función _push_back_.

		4. Agregar ĺa palabra "notas" al vector definido en el punto 2.
		   Utilizar la función _push_back_.

		5. Eliminar el último elemento del vector definido en el punto 1.
		   Utilizar la función _pop_back_.

		6. Eliminar el número 100 del vector definido en el punto 1. El
		   orden los elementos deberá mantenerse.

		7. Reemplazar el primer elemento por el número 1200.

		8. Sumar los valores correspondientes al primer y al último elemento.
		   Tomar el valor resultante y colocarlo como segundo elemento del vector. 
	
- Funciones con vectores:

	9. Definir una función que reciba un vector de números enteros
	como parámetro por copia. La función deberá retornar la cantidad
	de números mayores a 10 que contiene.

	10. Definir una función que reciba un vector de números enteros
	como parámetro por referencia. La función deberá duplicar a todos 
	los valores que sean impares.



	
