-----
#### Laboratorio de Algoritmos y Estructuras de Datos • Evaluación
-----
##### Strings, Funciones y Vectores
-----


1. Definir una función que reciba una palabra como parámetro
por valor y una letra como parámetro por valor.
La función deberá retornar true en caso de que la letra recibida
se encuentre en la palabra.

2. Definir una función que reciba una letra como parámetro
por copia y luego retorne
la letra en minúscula.

Verificar su funcionamiento realizando las siguientes pruebas:
	- Si la función recibe la letra 'a' deberá retornar la letra 'a'
	- Si la función recibe la letra 'A' deberá retornar la letra 'a'
	- Si la función recibe un dato de tipo char que no sea una letra,
	deberá imprimir un mensaje que indique lo sucedido y la función
	deberá finalizar su ejecución.

3. Definir una función que reciba una letra como parámetro
por referencia y luego la convierta en una letra en minúscula.


4. Definir una función que reciba una letra como parámetro
por copia y luego retorne
la letra en mayúscula.

Verificar su funcionamiento realizando las siguientes pruebas:

	- Si la función recibe la letra 'a' deberá retornar la letra 'A'
	- Si la función recibe la letra 'A' deberá retornar la letra 'A'
	- Si la función recibe un dato de tipo char que no sea una letra,
	deberá imprimir un mensaje que indique lo sucedido y la función
	deberá finalizar su ejecución.

5. Definir una función que reciba una letra como parámetro
por referencia y luego retorne
la letra en mayúscula.

6. Definir una función que reciba una palabra como parámetro
por copia y luego
retorne la misma palabra pero con todas sus letras en mayúsculas.

Verificar su funcionamiento realizando las siguientes pruebas:

	- Si la función recibe la palabra 'Casa' deberá retornar 'CASA'
	- Si la función reciba la palabra 'CASA' deberá retornar 'CASA'
	- Si la función reciba la palabra 'casa' deberá retornar 'CASA'

7. Definir una función que reciba una palabra como parámetro
por referencia y luego
convierta todas sus letras en mayúsculas.

8. Definir una función que reciba una palabra como parámetro
por copia y luego
retorne la misma palabra pero con todas sus letras en minúsculas.

Verificar su funcionamiento realizando las siguientes pruebas:

	- Si la función recibe la palabra 'Casa' deberá retornar 'casa'
	- Si la función reciba la palabra 'casa' deberá retornar 'casa'
	- Si la función reciba la palabra 'CASA' deberá retornar 'casa'




