-----
#### Laboratorio de Algoritmos y Estructuras de Datos • Evaluación
-----
##### Diagramas de flujo • Pseudocódigo
-----

- Proponer algoritmo que resuelvan los siguientes problemas. El algoritmo
propuesto deberá estar escrito con pseudocódigo y diagramas de flujo.


1. Problema: se necesita calcular el volumen de una esfera.
El algoritmo deberá recibir un valor de radio que será ingresado por el
usuario.

La fórmula para calcular el volumen de una esfera es: v = (4 * 3,14 * r * r * r) / 3


2. Problema: se necesita determinar si la tarjeta sube tiene saldo suficiente
para pagar el siguiente viaje. El programa deberá recibir el monto actual
de la sube y el valor del viaje que se desea realizar.
El saldo negativo actual es de $1080. En caso de que exista saldo suficiente
para realizar el viaje, deberá imprimir la frase "Puede realizar el viaje" y
deberá imprimir el nuevo saldo. En caso de que no exista saldo suficiente
para realizar el viaje, deberá imprimir la frase "No puede realizar el viaje" y
luego se deberá finalizar el algoritmo.


3. Problema: se necesita un programa que sume el importe total de una compra.
Por parte del usuario, el programa recibirá: cantidad total de productos
comprados, el precio de cada producto y la cantidad de unidades por producto.


4. Problema: se necesita un programa que le pida al usuario 500 números
y luego imprima la cantidad de números pares que ingresó.


5. Problema: se necesita un programa que le pida al usuario una cantidad 
indeterminada de nombres. Si el usuario ingresa la palabra 'finalizado'
deberá dejar de pedir nombres y luego imprimir la cantidad total
de nombres recibidos.


