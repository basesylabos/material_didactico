-----
#### Laboratorio de Algoritmos y Estructuras de Datos
-----
##### 25 de Junio • Recuperatorio
-----

1. Crear un programa que reciba por parte del usuario una cantidad indeterminada de palabras. El programa deberá generar una nueva cadena que esté compuesta por la primera y última letra de todas las palabras recibidas, separadas por un guión medio.
Si el usuario ingresa la palabra "final", deberá dejar de recibir palabras y luego deberá imprimir la cadena generada.

	- Ejemplo: si se ingresan las palabras _**c**as**a**_, _**t**eclad**o**_, _**m**ous**e**_ y
	  _**m**onito**r**_, entonces se deberá generar y luego imprimir la cadena _**ca-to-me-mr**_. 
      
	Tener en cuenta que la priemera y la última letra de la palabra final no deberá estar en la cadena generada.
    
    
2. Crear un programa que le pida al usuario 500 números y luego sume a aquellos que pertenezcan al conjunto A. 
Finalmente, el programa deberá imprimir el valor resultante. 

	``` 
    
     			     	    A = (-∞, 500] U [4000, 10800) 
    
    ```
                
3. Crear un programa donde el usuario ingrese un número negativo y luego ingrese un número positivo. El programa deberá sumar todos los números pares que se encuentran entre los dos números recibidos. Si los números recibidos son pares, también deberán ser parte de la suma calculada. Por último, el programa deberá imprimir el valor resultante.

	- Ejemplo: si se reciben los números -4 y el 2,
    se deberá imprimir el valor -4 ya que 
    ```- 4 + (- 2) + 0 + 2 = - 4```