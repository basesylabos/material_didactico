----
#### Laboratorio de Algoritmos y Estructuras de Datos
----

##### Ejercicios 


```c++

// ciclo while

int i = 0;

while (i < 3) {

  cout << i << endl;
  i++;

}

// ciclo for

for (int i = 0; i < 3; i++){
  cout << i << endl;
}


```

- Programar en c++ una solución para los problemas que fueron propuestos el 14 de Marzo:

	1. Mostrar los números desde el 1 al 1000.
	2. Sumar los primeros 100 números y luego mostrar el resultado de la suma.
	3. Mostrar los números desde el 100 al 5000.
	4. Pedirle al usuario una cantidad indeterminada de números y sumarlos. En caso de que el usuario ingrese el 0,
	ya no se deberá pedir números y luego se deberá mostrar el resultado de la suma.
	5. Pedirle al usuario un número mayor o igual a 1 y luego mostrar todos los números positivos que son menores o
	iguales al número recibido. Ejemplo: si el usuario ingresa el número 3, se deberán mostrar los números 1, 2 y 3.






	
	
