-----
#### Laboratorio de Algoritmos y Estructuras de Datos
-----
##### 2 de Julio • Ejemplos
-----


- Ejemplo para obtener números random

```c++
    
#include <iostream>

using namespace std;


int obtenerNumeroRandomEntero(int desde, int hasta) {
    int valor = desde + rand() % (hasta - desde + 1);
    return valor;
}

int main(){
    
    srand(time(NULL));
    
    for (int i = 1; i <= 200; i++) {
        cout << obtenerNumeroRandomEntero(1,10) << endl;
    }

    return 0;
}
    
```


