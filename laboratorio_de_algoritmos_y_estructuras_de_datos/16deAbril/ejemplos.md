----
#### Laboratorio de Algoritmos y Estructuras de Datos
----

##### Ejemplos


```c++

// Definición de funciones:


void saludar(){

	string nombre;
	cin >> nombre;
	cout << "Bienvenido, " << nombre << "." << endl;

}

string obtenerPrimeros10000numerosPositivos(){

	string primeros10milPositivos = "";

    for (int i = 0; i < 10000; ++i) {
        primeros10milPositivos += to_string(i);
        primeros10milPositivos += ' ';
    }

    return primeros10milPositivos;
}

// Definición de función con parámetro por copia o por valor

int duplicar(int numero){

	int resultado = numero * 2;
	return resultado;

}

// Definición de función con parámetro por referencia

void triplicar(int &numero){

	numero = numero * 3;
}

// Definición de función que recibe un vector y luego retorna el primer elemento 

int obtenerPrimerElemento(vector<int> numeros){
    int primerElemento = numeros[0];
    return primerElemento;
}


int main(){

	
	// utilización función que no recibe parámetros y no retorna datos
	saludar();




	// utilización función que no recibe parámetros y retorna un dato de tipo string
	string primerosPositivosComoCadena = obtenerPrimeros10000numerosPositivos();
	



	/*
	utilización función que recibe dato como 
	parámetro por copia/valor y retorna un
	dato de tipo entero
	*/
	int num1 = duplicar(10); // 20

	int num2 = 100;
	int resultado = duplicar(num2); // 200



	// utilización de función que recibe un dato como parámetro por referencia
	int num3 = 10;

	truplicar(num3);
	
	cout << num 3 << endl; // 30



	// vectores

	vector<int> listado_de_numeros = {4,2,1,4,2,2,1};

	vector<int> nums;
	nums.push_back(100);
	nums.push_back(20);
	nums.push_back(12);

	// nums = {100,20,12}

	nums.pop_back(); // {100,20}
	nums.pop_back(); // {100}

	// asignación

	listado_de_numeros = {1,4,1,12};

	vector<int> listado_de_numeros_2 = {1,7,66,54};
	listado_de_numeros = listado_de_numeros_2;

	// asignación de valores

	listado_de_numeros[0] = 22;
	listado_de_numeros[1] = 1;
	listado_de_numeros[2] = 4;
	listado_de_numeros[0] = listado_de_numeros[1]; // {1,1,4}

	

	// Utilización de funciones con vectores
	int primerElemento = obtenerPrimerElemento(listado_de_numeros); // 4

   return 0;
}
```