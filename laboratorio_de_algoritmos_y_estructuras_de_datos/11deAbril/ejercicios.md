----
#### Laboratorio de Algoritmos y Estructuras de Datos
----

##### Ejemplo


```c++

int main(){



	// Operaciones con strings

	string palabra = "casa";


	// cantidad de elementos del string

	int cantidad = palabra.size();



	// concatenación de strings

	palabra = palabra + palabra; // "casacasa"
	palabra += palabra; // "casacasacasa"



	// variables de tipo char

	char letra1 = 'a';
	char simbolo = '@';
	char numero = '4';


	// concatenación de letras

	palabra = "casa";
	palabra += 's'; // "casas"
	palabra += '2'; // "casas2"

	palabra = "monitor"

	// primera letra

	char primeraLetra = palabra[0]; // m

	// última letra
	int ultimoIndice = palabra.size() - 1;
	char ultimaLetra = palabra[ultimoIndice] // r

	// iterar sobre un dato de tipo string
	// ejemplo donde se imprimen las letras del dato que contiene la variable palabra

	for (int i = 0; i < palabra.size(); ++i)
	{
		char letra = palabra[i];
		cout << letra;
	}


   return 0;
}
```

##### Ejercicios

- Ejercicios que deben ser realizados sin utilizar ciclos:

	1. Programar una aplicación que reciba un nombre y luego imprima la primera letra.
	2. Programar una aplicación que reciba un nombre y luego imprima la última letra.
	3. Programar una aplicación que reciba un nombre que tenga más de 3 letras y luego imprima la
	letra con índice 1.
	4. Programar una aplicación que reciba un nombre que tenga más de 5 letras y luego imprima las
	letras que tienen índices 1, 3 y 0; en ese orden.

- Ejercicios que deben ser realizados utilizando ciclos:

	5. Programar una aplicación que reciba un nombre y luego imprima los elementos que tengan índices pares.
	6. Programar una aplicación que reciba un nombre y luego imprima una cadena que esté compuesta por
	todas las vocales que contiene el nombre recibido.
	7. Programar una aplicación que reciba un nombre y luego imprima una cadena compuesta
	por 1000 repeticiones del nombre recibido. Cada repetición deberá estar separada con un guión bajo.
	9. Programar una aplicación que reciba una cantidad indeterminada de 
	nombres y luego imprima una cadena compuesta por todos los nombres recibidos. Si el usuario ingresa la palabra "fin", deberá dejar de pedir nombres. La palabra "fin" no deberá estar dentro de la cadena generada.
	
	

	
